import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthServiceService } from '../auth/services/auth-service.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginActiveGuard implements CanActivate {
  constructor(private AuthService: AuthServiceService, private router: Router) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.AuthService.isLogged().pipe(
      map(
        result => {
          if (result === false) {
            this.router.navigate(['/login']);
          }
            return result;
          })
    );
  }
}
