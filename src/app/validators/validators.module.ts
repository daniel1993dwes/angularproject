import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OneCheckedDirective } from './one-checked.directive';

@NgModule({
  declarations: [
    OneCheckedDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    OneCheckedDirective
  ]
})
export class ValidatorsModule { }
