import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogoutActiveGuard } from './guards/logout-active.guard';
import { LoginActiveGuard } from './guards/login-active.guard';

const routes: Routes = [
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule', canActivate: [LogoutActiveGuard] },
  { path: 'profile', loadChildren: './users/users.module#UsersModule', canActivate: [LoginActiveGuard] },
  { path: 'restaurants', loadChildren: './restaurant/restaurants.module#RestaurantModule', canActivate: [LoginActiveGuard] },
  { path: '', redirectTo: '/auth/login', pathMatch: 'full' },
  { path: '**', redirectTo: '/auth/login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
