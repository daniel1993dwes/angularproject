import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../../auth/services/auth-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'fs-top-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  logged = false;
  constructor(private AuthService: AuthServiceService, private router: Router) { }

  ngOnInit() {
    this.AuthService.$loginChange.subscribe(result => this.logged = result);
  }

  logoutButton() {
    this.AuthService.logout();
    this.router.navigate(['/auth/login']);
  }

}
