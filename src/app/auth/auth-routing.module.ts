import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogoutActiveGuard } from '../guards/logout-active.guard';
import { LoginPageComponent } from './login-page/login-page.component';
import { RegisterComponent } from './register/register.component';
import { CanDeactivateFormGuard } from '../guards/can-deactivate.guard';

const routes: Routes = [
  {
    path: 'login',
    canActivate: [LogoutActiveGuard],
    component: LoginPageComponent
  },
  {
    path: 'register',
    canActivate: [LogoutActiveGuard],
    component: RegisterComponent,
    canDeactivate: [CanDeactivateFormGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
