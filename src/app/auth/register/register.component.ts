import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../interface/user';
import { NgModel, NgForm } from '@angular/forms';
import { AuthServiceService } from '../services/auth-service.service';
import { GeolocationService } from '../services/geolocation.service';
import { Title } from '@angular/platform-browser';
import { CanComponentDeactivate } from 'src/app/shared/interfaces/cancomponentDeactivate';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, from, of } from 'rxjs';
import { ModalWindowComponent } from 'src/app/shared/modal-window/modal-window.component';
import { switchMap, map, catchError } from 'rxjs/operators';

@Component({
  selector: 'fs-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, CanComponentDeactivate {
  email2: string;
  errorGuardado = false;
  error: string;
  user: User;
  correosIguales = 'form-control mb-2';
  @ViewChild('fileImage') inputFile: ElementRef;
  @ViewChild('registerForm') registerForm: NgForm;

  constructor(private router: Router,
    private UserService: AuthServiceService,
    private Geolocation: GeolocationService,
    private TitleService: Title,
    private ModalService: NgbModal) { }

  ngOnInit() {
    this.resetForm();
    this.TitleService.setTitle('Register | FoodScore');
    this.error = '';
  }

  cambiaCorreo() {
    if (this.user.email === this.email2) {
      this.correosIguales = 'form-control mb-2 ng-touched ng-valid';
    } else {
      this.correosIguales = 'form-control mb-2 ng-touched ng-invalid';
    }
  }

  canDeactivate(): Observable<boolean> {
    if (this.registerForm.dirty && this.registerForm.valid && !this.registerForm.submitted) {
      const modalRef = this.ModalService.open(ModalWindowComponent);
      modalRef.componentInstance.title = 'Save Register';
      modalRef.componentInstance.body = 'Do you want to save changes?';
      modalRef.componentInstance.botonTrue = 'Save changes';
      modalRef.componentInstance.botonFalse = 'Exit';

      return from(modalRef.result).pipe(
        switchMap(resp => {
          console.log(resp);
          if (resp) {
            this.user.email = this.email2;
            return this.UserService.register(this.user).pipe(
              map(p => {
                return true;
              }),
              catchError(e => {
                this.errorGuardado = true;
                this.error = e;
                return of(false);
              })
            );
          } else {
            return of(true);
          }
        }),
        catchError(e => of(false))
      );
    }

    return of(true);
  }

  changeImage(fileInput: HTMLInputElement) {
    if (!fileInput.files || fileInput.files.length === 0) { return; }
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(fileInput.files[0]);
    reader.addEventListener('loadend', e => {
      this.user.avatar = <string> reader.result;
    });
  }

  validClasses(ngModel: NgModel, validClass: string, errorClass: string) {
    return {
      [validClass]: ngModel.touched && ngModel.valid,
      [errorClass]: ngModel.touched && ngModel.invalid
    };
  }

  goToBack() {
    this.router.navigate(['/auth/login']);
  }

  resetForm() {
    this.user = {
      id: -1,
      name: '',
      email: '',
      password: '',
      lat: 0,
      lng: 0,
      avatar: '',
      token: '',
      me: false
    };
    this.email2 = '';
    this.Geolocation.getLocation().then(location => {
      this.user.lat = location.latitude;
      this.user.lng = location.longitude;
    });
  }

  addUser() {
    if (this.registerForm.invalid || !this.inputFile.nativeElement.files.length) { return; }

    this.user.email = this.email2;
      this.UserService.register(this.user).subscribe(
        result => { this.router.navigate(['/auth/login']); },
        error => { console.error(error); }
    );
  }

}
