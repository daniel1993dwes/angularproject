import { Component, OnInit, NgZone } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { User } from '../interface/user';
import { GeolocationService } from '../services/geolocation.service';

@Component({
  selector: 'fs-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  email: '';
  password: '';
  user: User;

  constructor(private TitleService: Title,
    private AuthService: AuthServiceService,
    private router: Router,
    private ngZone: NgZone,
    private Geolocation: GeolocationService) { }

  addLogin() {
    this.AuthService.login(this.email, this.password).subscribe(
      rest => {
        this.router.navigate(['/restaurants']);
      }
    );
  }

  goToRegister() {
    this.router.navigate(['/auth/register']);
  }

  ngOnInit() {
    this.TitleService.setTitle('Login | FoodScore');
    this.user = {
      id: -1,
      name: '',
      email: '',
      password: '',
      lat: 0,
      lng: 0,
      avatar: '',
      token: '',
      me: false
    };
    this.Geolocation.getLocation().then(location => {
      this.user.lat = location.latitude;
      this.user.lng = location.longitude;
    });
  }

   errorGoogle(error) {
    console.error(error);
  }

  loggedGoogle(user: gapi.auth2.GoogleUser) {
    this.ngZone.run(() => {
      this.user.token = user.getAuthResponse().id_token;
      this.AuthService.loginGoogle(this.user).subscribe(
        rest => {
          this.router.navigate(['/restaurants']);
        }
      );
    });
   }

   /**
    * Este metodo solo funciona en páginas https
    * @param resp Respuesta Facebook Login
    */
   loggedFacebook(resp: FB.LoginStatusResponse) {
    // Send this to your server
    this.ngZone.run(() => {
      this.user.token = resp.authResponse.accessToken;
      this.AuthService.loginFacebook(this.user).subscribe(
        rest => {
          this.router.navigate(['/restaurants']);
        }
      );
      });
  }

  showError(error) {
    console.error(error);
  }
}
