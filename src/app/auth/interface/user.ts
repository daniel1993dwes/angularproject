export interface User {
  id: number;
  name: string;
  email: string;
  password: string;
  lat: number;
  lng: number;
  avatar: string;
  token: string;
  me: boolean;
}
