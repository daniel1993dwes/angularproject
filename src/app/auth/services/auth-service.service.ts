import { Injectable, EventEmitter, Output } from '@angular/core';
import { Observable, throwError, of } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { User } from '../interface/user';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {
  logged = false;
  $loginChange = new EventEmitter<boolean>();

  constructor(private http: HttpClient) { }

  login(email: string, password: string): Observable<void> {
    return this.http.post<{accessToken: string}>
    ('auth/login', {email: email, password: password}).pipe(
      map(resp => {
        localStorage.setItem('token', resp.accessToken);
        this.logged = true;
        this.$loginChange.emit(this.logged);
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error Login. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  loginGoogle(user: User): Observable<void> {
    return this.http.post<{lat: number, lng: number, accessToken: string}>
    ('auth/google', {lat: user.lat, lng: user.lng, token: user.token}).pipe(
      map(resp => {
        localStorage.setItem('token', resp.accessToken);
        this.logged = true;
        this.$loginChange.emit(this.logged);
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error Login. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  loginFacebook(user: User): Observable<void> {
    return this.http.post<{lat: number, lng: number, accessToken: string}>
    ('auth/facebook', {lat: user.lat, lng: user.lng, token: user.token}).pipe(
      map(resp => {
        localStorage.setItem('token', resp.accessToken);
        this.logged = true;
        this.$loginChange.emit(this.logged);
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error Login. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  isLogged(): Observable<boolean> {
    if (this.logged === true) {
      return of(true);
    } else {
      const token = localStorage.getItem('token');
      if (this.logged === false && token === null) {
        return of(false);
      } else {
        return this.http.get<boolean>('auth/validate')
        .pipe(
          map(r => {
            this.logged = true;
            this.$loginChange.emit(this.logged);
            return true;
          }), catchError((error: HttpErrorResponse) =>
          throwError(`Error isLogged. Status ${error.status}. Message: ${error.message}`)
          )
        );
      }
    }
  }

  logout() {
    localStorage.removeItem('token');
    this.logged = false;
    this.$loginChange.emit(this.logged);
  }

  register(user: User) {
    return this.http.post<{usuario: User}>('auth/register', user).pipe(
      map(
        resp => {
          return resp.usuario;
        }), catchError((error: HttpErrorResponse) =>
        throwError(`Error adding Restaurant. Status ${error.status}. Message: ${error.message}`)
        )
    );
  }
}
