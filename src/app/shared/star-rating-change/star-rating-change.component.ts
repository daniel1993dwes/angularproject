import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'star-rating-change',
  templateUrl: './star-rating-change.component.html',
  styleUrls: ['./star-rating-change.component.css']
})
export class StarRatingChangeComponent implements OnInit, OnChanges {
  @Input() rating: number;
  auxRating: number;
  @Output() changeRating = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
    this.auxRating = this.rating;
  }

  // This method is called when @Input properties change (rating)
  ngOnChanges() {
    this.auxRating = this.rating;
  }

  change() {
    this.changeRating.emit(this.auxRating);
  }

}
