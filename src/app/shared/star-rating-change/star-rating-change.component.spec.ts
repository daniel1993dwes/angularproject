import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StarRatingChangeComponent } from './star-rating-change.component';

describe('StarRatingComponent', () => {
  let component: StarRatingChangeComponent;
  let fixture: ComponentFixture<StarRatingChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StarRatingChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StarRatingChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
