import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalWindowComponent } from './modal-window/modal-window.component';
import { StarRatingComponent } from './star-rating/star-rating.component';
import { StarRatingChangeComponent } from './star-rating-change/star-rating-change.component';

@NgModule({
  declarations: [
    ModalWindowComponent,
    StarRatingComponent,
    StarRatingChangeComponent
  ],
  entryComponents: [
    ModalWindowComponent
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    ModalWindowComponent,
    StarRatingComponent,
    StarRatingChangeComponent
  ]
})
export class SharedModule { }
