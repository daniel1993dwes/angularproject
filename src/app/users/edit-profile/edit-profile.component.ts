import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { User } from 'src/app/auth/interface/user';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from '../services/users.service';
import { environment } from '../../../environments/environment';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'fs-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {
  restUrl = environment.baseUrl;
  user: User;
  modifiedUser: User;
  avatar: string;
  errorCambiarNombre = false;
  exitoCambiarNombre = false;
  errorCambiarPassword = false;
  exitoCambiarPassword = false;
  errorCambiarAvatar = false;
  exitoCambiarAvatar = false;
  error1: string;
  error2: string;
  error3: string;
  exito1: string;
  exito2: string;
  exito3: string;
  contrasenasIguales: string;
  password2: string;

  @ViewChild('nameForm') nameForm: NgForm;
  @ViewChild('passwordForm') passwordForm: NgForm;
  @ViewChild('avatarForm') avatarForm: NgForm;
  @ViewChild('fileImage') fileImage: ElementRef;
  constructor(private route: ActivatedRoute,
    private UserService: UsersService) { }

  ngOnInit() {
    this.user = this.route.snapshot.data['profile'];
    this.modifiedUser = this.user;
    console.log(this.modifiedUser);
    this.resetVariables();
    this.contrasenasIguales = 'form-control';
  }

  cambiaContrasena() {
    console.log(this.modifiedUser.password);
    console.log(this.password2);
    if (this.modifiedUser.password === this.password2) {
      this.contrasenasIguales = 'form-control mb-2 ng-touched ng-valid';
      return true;
    } else {
      this.contrasenasIguales = 'form-control mb-2 ng-touched ng-invalid';
      return false;
    }
  }

  changeImage(fileInput: HTMLInputElement) {
    if (!fileInput.files || fileInput.files.length === 0) { return; }
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(fileInput.files[0]);
    reader.addEventListener('loadend', e => {
      this.avatar = <string> reader.result;
    });
  }

  cambiarCorreoYNombre() {
    if (this.nameForm.invalid) {
      this.resetVariables();
      this.errorCambiarPassword = true;
      this.error3 = 'Campos no completados';
      return;
    }
    this.UserService.putUserMe(this.modifiedUser).subscribe(
      result => {
        this.resetVariables();
        this.exitoCambiarNombre = true;
        this.exito1 = 'Nombre y Correo cambiado con exito!!';
      },
      error => {
        this.resetVariables();
        this.errorCambiarNombre = true;
        this.error1 = 'No se ha podido cambiar el nombre y el correo';
      }
    );
  }

  cambiarPassword() {
    if (!this.cambiaContrasena()) {
      this.resetVariables();
      this.errorCambiarPassword = true;
      this.error3 = 'Contraseñas deben ser iguales';
      return;
    }
    this.UserService.putUserMePassword(this.modifiedUser).subscribe(
      result => {
        this.resetVariables();
        this.exitoCambiarPassword = true;
        this.exito3 = 'Contraseña cambiada con exito!!';
      },
      error => {
        this.resetVariables();
        this.errorCambiarPassword = true;
        this.error3 = 'No se ha podido cambiar la contraseña';
      }
    );
  }

  cambiarAvatar() {
    if (this.avatarForm.invalid || !this.fileImage.nativeElement.files.length) {
      this.resetVariables();
      this.errorCambiarPassword = true;
      this.error3 = 'Campos no completados';
      return;
    }
    this.modifiedUser.avatar = this.avatar;
    this.UserService.putUserMeAvatar(this.modifiedUser).subscribe(
      result => {
        this.resetVariables();
        this.exitoCambiarAvatar = true;
        this.exito2 = 'Se ha cambiado la imagen con exito!!';
        this.avatar = '';
      },
      error => {
        this.resetVariables();
        this.errorCambiarAvatar = true;
        this.error2 = 'No se ha podido cambiar el avatar';
        this.avatar = '';
      }
    );
  }
  resetVariables() {
    this.errorCambiarNombre = false;
    this.exitoCambiarNombre = false;
    this.errorCambiarPassword = false;
    this.exitoCambiarPassword = false;
    this.errorCambiarAvatar = false;
    this.exitoCambiarAvatar = false;
    this.error1 = '';
    this.error2 = '';
    this.error3 = '';
    this.exito1 = '';
    this.exito2 = '';
    this.exito3 = '';
    this.avatar = '';
  }
}
