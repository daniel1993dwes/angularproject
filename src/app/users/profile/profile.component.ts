import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/auth/interface/user';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'fs-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: User;
  zoom: number;
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.user = this.route.snapshot.data['profile'];
    this.zoom = 17;
  }

}
