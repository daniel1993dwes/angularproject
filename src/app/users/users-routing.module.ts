import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { LoginActiveGuard } from '../guards/login-active.guard';
import { UserResolverService } from './services/user-resolver.service';
import { EditProfileComponent } from './edit-profile/edit-profile.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
    canActivate: [LoginActiveGuard],
    resolve: {
      profile: UserResolverService
    }
  },
  {
    path: 'edit',
    component: EditProfileComponent,
    canActivate: [LoginActiveGuard],
    resolve: {
      profile: UserResolverService
    }
  },
  {
    path: ':id',
    component: ProfileComponent,
    canActivate: [LoginActiveGuard],
    resolve: {
      profile: UserResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
