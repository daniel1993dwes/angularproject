import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestaurantResponse } from '../interface/restaurantResponse';
import { Comment } from '../interface/comment';
import { Title } from '@angular/platform-browser';
import { RestaurantServiceService } from '../services/restaurant-service.service';

@Component({
  selector: 'fs-restaurant-details',
  templateUrl: './restaurant-details.component.html',
  styleUrls: ['./restaurant-details.component.scss']
})
export class RestaurantDetailsComponent implements OnInit {
  restaurant: RestaurantResponse;
  zoom: number;
  commentsGeters: Comment[] = [];
  comment: Comment;

  constructor(private route: ActivatedRoute,
    private TitleService: Title,
    private RestaurantService: RestaurantServiceService) { }

  ngOnInit() {
    this.restaurant = this.route.snapshot.data['restaurant'];
    this.TitleService.setTitle('Details | FoodScore');
    this.zoom = 17;
    this.comment = {
      id: -1,
      stars: 0,
      text: '',
      restaurantId: -1,
      user: {
        id: -1,
        name: '',
        email: '',
        password: '',
        lat: 0,
        lng: 0,
        avatar: '',
        token: '',
        me: false
      },
      date: null
    };
    this.getComments();
  }

  getComments() {
    this.RestaurantService.getComments(this.restaurant.id).subscribe(
      result => {
        this.commentsGeters = result;
      }
    );
  }

  changeRating(newRating) {
    this.comment.stars = newRating;
  }

  addComment() {
    if (this.comment.stars === 0) {
      this.comment.stars = 1;
    }
    this.RestaurantService.addComment(this.restaurant.id, this.comment).subscribe(
      result => {
        this.commentsGeters.push(result);
        this.restaurant.commented = true;
      }
    );
  }
}
