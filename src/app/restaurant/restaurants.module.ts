import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RestaurantRoutingModule } from './restaurant-routing.module';
import { FormsModule } from '@angular/forms';
import { RestaurantDetailsComponent } from './restaurant-details/restaurant-details.component';
import { RestaurantCardComponent } from './restaurant-card/restaurant-card.component';
import { RestaurantsPageComponent } from './restaurants-page/restaurants-page.component';
import { RestaurantFormComponent } from './restaurant-form/restaurant-form.component';
import { RestaurantFilterPipe } from './pipes/restaurant-filter.pipe';
import { ValidatorsModule } from '../validators/validators.module';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { SharedModule } from '../shared/shared.module';
import { CommentComponent } from './comment/comment.component';

@NgModule({
  declarations: [
    RestaurantDetailsComponent,
    RestaurantCardComponent,
    RestaurantsPageComponent,
    RestaurantFormComponent,
    RestaurantFilterPipe,
    CommentComponent,
  ],
  imports: [
    CommonModule,
    RestaurantRoutingModule,
    FormsModule,
    ValidatorsModule,
    NgxMapboxGLModule,
    SharedModule
  ]
})
export class RestaurantModule { }
