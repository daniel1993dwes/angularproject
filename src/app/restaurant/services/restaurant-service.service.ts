import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { RestaurantResponse } from '../interface/restaurantResponse';
import { Comment } from '../interface/comment';

@Injectable({
  providedIn: 'root'
})
export class RestaurantServiceService {
restURL = environment.baseUrl;
  constructor(private http: HttpClient) { }

  getComments(id: number): Observable<Comment[]> {
    return this.http.get<{comments: Comment[]}>('restaurants/' + id + '/comments')
    .pipe(
      map(resp => {
        return resp.comments.map(e => {
           e.user.avatar = this.restURL + '/' + e.user.avatar;
           return e;
        });
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting Comments. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  addComment(id: number, comment: Comment): Observable<Comment> {
    return this.http.post<{comment: Comment}>('restaurants/' + id + '/comments', comment)
    .pipe(
      map(resp => {
        return resp.comment;
      }), catchError((error: HttpErrorResponse) =>
        throwError(`Error posting Comment. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  getRestaurants(): Observable<RestaurantResponse[]> {
    return this.http.get<{restaurants: RestaurantResponse[]}>('restaurants')
    .pipe(
      map(resp => {
        return resp.restaurants.map(r => {
          r.image = this.restURL + '/' + r.image;
          return r;
        });
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting Restaurants. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  getRestaurant(id: number): Observable<RestaurantResponse> {
    return this.http.get<{restaurant: RestaurantResponse}>('restaurants/' + id)
    .pipe(
      map(resp => {
        resp.restaurant.image = this.restURL + '/' + resp.restaurant.image;
        resp.restaurant.daysOpen.map(d => Number(d));
        return resp.restaurant;
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting Restaurant ID. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  getRestaurantsMine(): Observable<RestaurantResponse[]> {
    return this.http.get<{restaurants: RestaurantResponse[]}>('restaurants/mine')
    .pipe(
      map(resp => {
        return resp.restaurants.map(r => {
          r.image = this.restURL + '/' + r.image;
          return r;
        });
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting Restaurant ID. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  getRestaurantsUser(id: number): Observable<RestaurantResponse[]> {
    return this.http.get<{restaurants: RestaurantResponse[]}>('restaurants/user/' + id)
    .pipe(
      map(resp => {
        return resp.restaurants.map(r => {
          r.image = this.restURL + '/' + r.image;
          return r;
        });
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting Restaurant ID. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  addRestaurant(rest: RestaurantResponse): Observable<RestaurantResponse> {
    return this.http.post<{restaurant: RestaurantResponse}>('restaurants', rest).pipe(
      map(
        resp => {
          return resp.restaurant;
        }), catchError((error: HttpErrorResponse) =>
        throwError(`Error adding Restaurant. Status ${error.status}. Message: ${error.message}`)
        )
    );
  }

  putRestaurant(id: number, rest: RestaurantResponse): Observable<RestaurantResponse> {
    return this.http.put<{restaurant: RestaurantResponse}>('restaurants/' + id, rest).pipe(
      map(
        resp => {
          return resp.restaurant;
        }), catchError((error: HttpErrorResponse) =>
        throwError(`Error updating Restaurant. Status ${error.status}. Message: ${error.message}`)
        )
    );
  }

  deleteRestaurant(id: number): Observable<number> {
    return this.http.delete<{identifier: number}>('restaurants/' + id).pipe(
      map(
        resp => {
          return resp.identifier;
        }), catchError((error: HttpErrorResponse) =>
        throwError(`Error deleting Restaurant. Status ${error.status}. Message: ${error.message}`)
        )
    );
  }
}
