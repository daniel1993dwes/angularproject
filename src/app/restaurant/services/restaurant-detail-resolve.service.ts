import { Injectable } from '@angular/core';
import { RestaurantResponse } from '../interface/restaurantResponse';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { RestaurantServiceService } from './restaurant-service.service';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestaurantDetailResolveService implements Resolve<RestaurantResponse> {

  constructor(private RestaurantService: RestaurantServiceService,
    private router: Router) { }

  resolve(route: ActivatedRouteSnapshot): Observable<RestaurantResponse> {
    return this.RestaurantService.getRestaurant(route.params['id']).pipe(
      catchError(error => {
        this.router.navigate(['/restaurants']);
        return of(null);
      })
      );
  }
}
