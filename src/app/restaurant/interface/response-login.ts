export interface ResponseLogin {
  expiresIn: number;
  accessToken: string;
}
