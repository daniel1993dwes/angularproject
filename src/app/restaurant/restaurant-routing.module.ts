import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RestaurantDetailsComponent } from './restaurant-details/restaurant-details.component';
import { LoginActiveGuard } from '../guards/login-active.guard';
import { RestaurantDetailResolveService } from './services/restaurant-detail-resolve.service';
import { RestaurantFormComponent } from './restaurant-form/restaurant-form.component';
import { RestaurantsPageComponent } from './restaurants-page/restaurants-page.component';
import { CanDeactivateFormGuard } from '../guards/can-deactivate.guard';

const routes: Routes = [
  {path: 'edit/:id',
  component: RestaurantFormComponent,
  canActivate: [LoginActiveGuard],
  canDeactivate: [CanDeactivateFormGuard],
  resolve: {
    restaurant: RestaurantDetailResolveService
    }
  },
  {path: 'details/:id',
  component: RestaurantDetailsComponent,
  canActivate: [LoginActiveGuard],
  resolve: {
    restaurant: RestaurantDetailResolveService
    }
  },
  {
    path: 'add',
    component: RestaurantFormComponent,
    canActivate: [LoginActiveGuard],
    canDeactivate: [CanDeactivateFormGuard]
  },
  {
    path: '',
    component: RestaurantsPageComponent,
    canActivate: [LoginActiveGuard]
  },
  {
    path: 'mine',
    component: RestaurantsPageComponent,
    canActivate: [LoginActiveGuard]
  },
  {
    path: 'user/:id',
    component: RestaurantsPageComponent,
    canActivate: [LoginActiveGuard]
  }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestaurantRoutingModule { }
