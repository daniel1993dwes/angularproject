import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { RestaurantResponse } from '../interface/restaurantResponse';
import { constants } from '../../constants/constants';
import { Router, ActivatedRoute } from '@angular/router';
import { RestaurantServiceService } from '../services/restaurant-service.service';
import { NgModel, NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Result } from 'ngx-mapbox-gl/lib/control/geocoder-control.directive';
import { GeolocationService } from 'src/app/auth/services/geolocation.service';
import { CanComponentDeactivate } from 'src/app/shared/interfaces/cancomponentDeactivate';
import { from, of, Observable } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalWindowComponent } from 'src/app/shared/modal-window/modal-window.component';

@Component({
  selector: 'fs-restaurant-form',
  templateUrl: './restaurant-form.component.html',
  styleUrls: ['./restaurant-form.component.scss']
})
export class RestaurantFormComponent implements OnInit, CanComponentDeactivate {
  restaurant: RestaurantResponse;
  newRest: RestaurantResponse;
  zoom: number;
  cuisine = '';
  readonly days = constants.days;
  daysOpen = constants.daysOpen;
  errorGuardado = false;
  error: string;
  controlDeImagen = 'form-control';

  @ViewChild('fileImage') inputFile: ElementRef;
  @ViewChild('restaurantForm') restForm: NgForm;

  constructor(private router: Router,
    private RestaurantService: RestaurantServiceService,
    private TitleService: Title,
    private Geolocation: GeolocationService,
    private ModalService: NgbModal,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.restaurant = this.route.snapshot.data['restaurant'];
    if (this.restaurant === undefined) {
      this.resetForm();
      this.Geolocation.getLocation().then(location => {
        this.newRest.lat = location.latitude;
        this.newRest.lng = location.longitude;
      });
    } else {
      this.newRest = this.restaurant;
      this.cuisine = this.newRest.cuisine.join(',');
      this.newRest.lat = Number(this.newRest.lat);
      this.newRest.lng = Number(this.newRest.lng);
    }
    this.TitleService.setTitle('Add Restaurant | FoodScore');
    this.zoom = 17;
    this.error = '';
  }

  canDeactivate(): Observable<boolean> {
    if (this.restForm.dirty  && this.restForm.valid && !this.restForm.submitted) {
      const modalRef = this.ModalService.open(ModalWindowComponent);
      modalRef.componentInstance.title = 'Save Register';
      modalRef.componentInstance.body = 'Do you want to save changes?';
      modalRef.componentInstance.botonTrue = 'Save changes';
      modalRef.componentInstance.botonFalse = 'Exit';

      return from(modalRef.result).pipe(
        switchMap(resp => {
          if (resp) {
            return this.RestaurantService.addRestaurant(this.newRest).pipe(
              map(p => {
                return true;
              }),
              catchError(e => {
                this.errorGuardado = true;
                this.error = e;
                return of(false);
              })
            );
          } else {
            return of(true);
          }
        }),
        catchError(e => of(false))
      );
    } else {
      this.errorGuardado = true;
      this.error = 'Algun campo no ha sido introducido';
    }
    return of(true);
  }

  validClasses(ngModel: NgModel, validClass: string, errorClass: string) {
    return {
      [validClass]: ngModel.touched && ngModel.valid,
      [errorClass]: ngModel.touched && ngModel.invalid
    };
  }

  resetForm() {
    this.newRest = {
      id: -1,
      name: '',
      image: this.inputFile.nativeElement.value = '',
      cuisine: [],
      description: '',
      phone: '',
      daysOpen: [],
      address: '',
      lat: 0,
      lng: 0,
      stars: '',
      creator: {
        id: -1,
        name: '',
        email: '',
        password: '',
        lat: 0,
        lng: 0,
        avatar: '',
        token: '',
        me: false
      },
      mine: false,
      commented: false,
      distance: 0
    };
  }

  changePosition(result: Result) {
    this.newRest.lat = result.geometry.coordinates[1];
    this.newRest.lng = result.geometry.coordinates[0];
    this.newRest.address = result.place_name;
  }

  changeImage(fileInput: HTMLInputElement) {
    if (!fileInput.files || fileInput.files.length === 0) {
      this.controlDeImagen = 'form-control ng-touched ng-invalid';
      return;
    }
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(fileInput.files[0]);
    reader.addEventListener('loadend', e => {
      this.newRest.image = <string> reader.result;
      this.controlDeImagen = 'form-control ng-touched ng-valid';
    });
  }

  addRestaurant() {
    if (this.restForm.invalid || this.newRest.image === '') { return; }

    if (this.restaurant === undefined) {
      this.newRest.cuisine = this.cuisine.split(',');
      this.newRest.daysOpen = this.daysOpen.reduce((days, isSelected, i) => isSelected ? [...days, i] : days, []);
      this.RestaurantService.addRestaurant(this.newRest).subscribe(
        result => { this.router.navigate(['/restaurants']); },
        error => { console.error(error); }
      );
    } else {
      this.newRest.cuisine = this.cuisine.split(',');
      this.newRest.image = this.newRest.image.replace('img\\', 'img/');
      this.newRest.daysOpen = this.daysOpen.reduce((days, isSelected, i) => isSelected ? [...days, i] : days, []);
      this.RestaurantService.putRestaurant(this.newRest.id, this.newRest).subscribe(
        result => { this.router.navigate(['/restaurants']); },
        error => { console.error(error); }
      );
    }
  }

}
