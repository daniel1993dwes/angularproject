import { Component, OnInit } from '@angular/core';
import { RestaurantResponse } from './../interface/restaurantResponse';
import { RestaurantServiceService } from '../services/restaurant-service.service';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'fs-restaurants-page',
  templateUrl: './restaurants-page.component.html',
  styleUrls: ['./restaurants-page.component.scss']
})
export class RestaurantsPageComponent implements OnInit {
  restaurants: RestaurantResponse[] = [];
  newRest: RestaurantResponse;
  orderByName: boolean;
  showOpen: boolean;
  search = '';

  constructor(private RestaurantService: RestaurantServiceService,
    private TitleService: Title,
    private router: Router,
    private ActiveRoute: ActivatedRoute) { }

  ngOnInit() {
    const id = this.ActiveRoute.snapshot.params['id'];
    if (this.router.url.includes('mine')) {
      this.RestaurantService.getRestaurantsMine().subscribe(
        result => {
          this.restaurants = result;
        }
      );
    } else if (this.router.url.includes('user')) {
      this.RestaurantService.getRestaurantsUser(id).subscribe(
        result => {
          this.restaurants = result;
        }
      );
    } else {
      this.RestaurantService.getRestaurants().subscribe(
        results => {
          this.restaurants = results;
      },
        error => { console.error(error); },
      );
    }
    this.TitleService.setTitle('Index | FoodScore');
  }

  togleOrderName(event: Event) {
    event.preventDefault();
    this.orderByName = !this.orderByName;
  }

  toggleShowOpen(event: Event) {
    event.preventDefault();
    this.showOpen = !this.showOpen;
  }

  delete(rest: RestaurantResponse) {
    this.RestaurantService.deleteRestaurant(rest.id).subscribe(
      result => true,
      error => console.error(error)
    );
  }
}

