import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { RestaurantResponse } from '../interface/restaurantResponse';
import { constants } from '../../constants/constants';
import { ModalWindowComponent } from 'src/app/shared/modal-window/modal-window.component';
import { Observable, from, of } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'fs-restaurant-card',
  templateUrl: './restaurant-card.component.html',
  styleUrls: ['./restaurant-card.component.scss']
})
export class RestaurantCardComponent implements OnInit {
  readonly days = constants.days;
  daysOpen = constants.daysOpen;
  weekDay = constants.weekday;

  @ViewChild('restaurantDiv') restaurantDiv: ElementRef;
  @Input() restaurant: RestaurantResponse;
  @Output() delete = new EventEmitter<void>();

  constructor(private ModalService: NgbModal) { }

  ngOnInit() {
    this.weekDay = new Date().getDay();
    this.restaurant.distance = Number(this.restaurant.distance.toFixed(2));
  }

  deleteButton() {
      const modalRef = this.ModalService.open(ModalWindowComponent);
      modalRef.componentInstance.title = 'Delete Item';
      modalRef.componentInstance.body = 'Do you want to delete this item?';
      modalRef.componentInstance.botonTrue = 'Delete';
      modalRef.componentInstance.botonFalse = 'Exit';

      from(modalRef.result).subscribe(resp => {
          if (resp) {
            this.delete.emit();
            this.restaurantDiv.nativeElement.remove();
            return of(true);
          } else {
            return of(false);
          }
        });
  }
}
