import { Pipe, PipeTransform } from '@angular/core';
import { RestaurantResponse } from '../interface/restaurantResponse';

@Pipe({
  name: 'restaurantFilter'
})
export class RestaurantFilterPipe implements PipeTransform {

  transform(restaurants: RestaurantResponse[],
    orderByName: boolean = false, showOpen: boolean = false, search: string = ''): RestaurantResponse[] {
    let result = [];
    result = restaurants;
    if (showOpen) {
        result = restaurants.filter(e => {
            if (e.daysOpen.includes(new Date().getDay().toString())) {
                return e;
            }
        });
    }if (search !== '' ) {
        result = result.filter(e => {
            if (e.name.toLowerCase().indexOf(search.toLowerCase()) !== -1) {
                return e;
            }
        });
    }
    if (orderByName) {
        result.sort((n1, n2): number => {
            if (n1.name < n2.name) {
              return -1;
            }
            if (n1.name > n2.name) {
              return 1;
            }
            return 0;
        });
    }if (!orderByName) {
        result.sort((n1, n2): number => {
            if (n1.id < n2.id) {
              return -1;
            }
            if (n1.id > n2.id) {
              return 1;
            }
            return 0;
        });
    }
    return result;
  }
}
