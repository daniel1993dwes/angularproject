import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthTokenInterceptor } from './interceptor/auth-token.interceptor';
import { MenuModule } from './menu/menu.module';
import { BaseUrlInterceptor } from './interceptor/base-url.interceptor';
import { SharedModule } from './shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GoogleLoginModule } from './login-google/google-login.module';
import { FacebookLoginModule } from './facebook-login/facebook-login.module';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MenuModule,
    SharedModule,
    NgbModule,
    GoogleLoginModule.forRoot('557331834920-vj2qta552qlmdbdsm5meh1lpl0dd2sod.apps.googleusercontent.com'),
    FacebookLoginModule.forRoot({
      app_id: '574507216345866',
      version: 'v3.2'
    }),
    NgxMapboxGLModule.withConfig({
      accessToken: 'pk.eyJ1IjoiYXJ0dXJvYmVyIiwiYSI6ImNqb2l2ZDFhZTA0azkzcHAyMGprcm04a3MifQ.2YFN4pXKpQmAPuVgLExW1w'
    })
  ],
  providers: [
    Title,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BaseUrlInterceptor,
      multi: true,
    }, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthTokenInterceptor,
    multi: true,
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
